#!/bin/bash

#Initial setup
# 1) Make 1_al.in unitcell input file. Note the vacancy unit cell in this instance is the 31 atom 2x2x2 perfect cell.
# -- The lattice parameter should be the relaxed one.
# -- Set it in Bohr so phonopy knows how to deal with it.
# 2) Make supercells with displacements: phonopy --qe -d --dim="1 1 1" -c  1_al.in. This will produce 7 displacements.
# 3) Make al_header.in file containing supercell calculation information

#Examplhttps://atztogo.github.io/phonopy/qe.htmle from the phonopy documentation can be found here:
#https://atztogo.github.io/phonopy/qe.html
#Within input files here:
#https://github.com/atztogo/phonopy/blob/master/example/NaCl-pwscf/NaCl-001.in

#Set the second index in the brace expansion to the number of supercells
batchJobs=4
c=0
for i in {001..007}; do
	cat al_header.in supercell-$i.in > 2_al_ph_$i.in
	pw.x < 2_al_ph_$i.in > 2_al_ph_$i.out &

#	wait
	#Wait for batches of $batchJobs to complete before starting next batch
	let c=c+1
	if (( $c % $batchJobs == 0 )); then wait; fi

done
wait
phonopy --qe -f 2_al_ph_{001..007}.out
wait
phonopy --qe -c 1_al.in -p band.conf
phonopy --qe -c 1_al.in -p mesh.conf

