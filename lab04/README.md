Lab 4: Surface DFT calculations
===============================

[Back to Course Overview](..)

Objectives
----------
- Make a vacuum slab manually and compute the surface formation energy. 
- Converge the slab width and the vacuum gap.
- Calculate the relaxation of the surface.
- Use the python Atomic Simulation Environment to create vacuum-slab for low Miller index surfaces. 
- Calculate surface adsorption energies. 
- Calculate ZrC surface energies and crystal facets in terms of Wulff construction.

Surface DFT calculation
----------------------

Perform SCF calculations for the conventional unitcell, and for the low Miller index sufaces. 

![vac_slab](.images/vac_slab_im.png)

#### Al unitcell
Create a directory called `01_al_unitcell`, containing an input file `1_al.in`. The structure should have a lattice constant of `a=4.04292971 Angstrom`, which is the value we found  minimising a 5th order polynomial in our [python jupyter notebook](lab02/.toMake/07_al_lattice_constant/al_lattice_parameter.pdf). The Al unit cell input, `1_al.in`, file should look like:
```
&CONTROL
   pseudo_dir = '.'
   calculation = 'scf'
/

&SYSTEM
   ibrav = 0      ! Bravais Lattice index -- 0 for free (give cell parameters)
   nat = 4        ! Num of atoms in cell
   ntyp = 1      ! Num of atom types
   ecutwfc = 29.0 ! Cutoff energy in Rydberg
   occupations='smearing'! Smearing occupation for metals
   degauss = 1.0d-01     ! Smearing width in Ry
   smearing='mp'         ! Methfessel-Paxton smearing
/

&ELECTRONS
   conv_thr = 1.0d-8  ! SCF convergence in Ry
   mixing_beta = 0.7  ! SCF mixing factor
   diagonalization = 'david'  ! Davidson Hamiltonian diagonalisation method
/

K_POINTS automatic
4 4 4 1 1 1

CELL_PARAMETERS (Angstrom)
4.042929710000000     0.000000000000000    0.000000000000000
0.000000000000000     4.042929710000000    0.000000000000000
0.000000000000000     0.000000000000000    4.042929710000000

ATOMIC_SPECIES
 Al   26.98154   Al.pbe-nl-kjpaw_psl.1.0.0.UPF

ATOMIC_POSITIONS crystal ! fractional positions
Al 0.00 0.00 0.00
Al 0.50 0.50 0.00
Al 0.00 0.50 0.50
Al 0.50 0.00 0.50
```
Compute the total energy for this structure the usual way, `pw.x < 1_al.in > 1_al.out &`. 

For our surface calculations, we will use to two methods. Firstly setting the cell up manually, then secondly, using the python Atomic Simulation Environment (ASE). 

In the first instance, copy the unitcell calculation to a new directory `02_surface_manual_scf`. Now edit the input file to make a 1x1x2 slab with a vacuum gap also equal to x2 the lattice parameter, with the surface orthogonal to the conventional unitcell c lattice vector. To adapt the unit cell input file, consider:
1. The number of atoms, `nat = `
2. k-point sampling. If we have a factor of four increase in the real-space length of the cell (x2 for the slab and x2 for the vacuum), how should we adapt reciprocal space sampling so that it remains commensurate to the other directions.
4. The atomic positions have to be modified. 

With these considerations in mind lets make a 1x1x2 slab with 1x1x2 vacuum. The finished input file should look something like:

```
&CONTROL
   pseudo_dir = '.'
   calculation = 'scf'
/

&SYSTEM
   ibrav = 0      ! Bravais Lattice index -- 0 for free (give cell parameters)
   nat = 4        ! Num of atoms in cell
   ntyp = 1      ! Num of atom types
   ecutwfc = 29.0 ! Cutoff energy in Rydberg
   occupations='smearing'! Smearing occupation for metals
   degauss = 1.0d-01     ! Smearing width in Ry
   smearing='mp'         ! Methfessel-Paxton smearing
/

&ELECTRONS
   conv_thr = 1.0d-8  ! SCF convergence in Ry
   mixing_beta = 0.7  ! SCF mixing factor
   diagonalization = 'david'
/

K_POINTS automatic
4 4 1 1 1 1

CELL_PARAMETERS (Angstrom)
4.042929710000000     0.000000000000000    0.000000000000000
0.000000000000000     4.042929710000000    0.000000000000000
0.000000000000000     0.000000000000000    16.17171884000000

ATOMIC_SPECIES
 Al   26.98154   Al.pbe-nl-kjpaw_psl.1.0.0.UPF

ATOMIC_POSITIONS crystal ! fractional positions
Al 0.00 0.00 0.00
Al 0.50 0.50 0.00
Al 0.00 0.50 0.125
Al 0.50 0.00 0.125
Al 0.00 0.00 0.250
Al 0.50 0.50 0.250
Al 0.00 0.50 0.375
Al 0.50 0.00 0.375
```


#### _Task_

1. Run the surface DFT calculation as an SCF calculation. 
2. Calculate the surface formation energy:
--  E(surface) = (E(vacuum-slab) - E(perfect) ) / (2 times  area of surface)
3. Repeat the calculation but this time allow the surface to relax. Do this by changing the calculation type to `relax`.
4. Determine whether the slab has relaxed inwards or outwards normal the surface.

### 100, 110 and 111 surface formation energies using the ASE

Open a jupyter notebook and follow the ASE calculations [here](vacuum-slab.pdf).

### Al 111 surface oxygen adsorption
1. `wget` the PAW PBE `O.pbe-n-kjpaw_psl.1.0.0.UPF` from the quantum-espresso pseudopotential (repository)[https://www.quantum-espresso.org/pseudopotentials]. 
2. Place oxygen atoms on both sides of the slab, and allow oxygen atoms to relax. Determine the surface adsorption energy with respect to gas phase oxygen (diatomic oxygen).
3.  Compute relative surface free energy of 1 ML and 4 ML oxidised Al surfaces as a function of chemical potential, following python notebook [here](vacuum-slab.pdf).

### ZrC -- _Task_

#### Surface formation energies for ZrC
- By analogy with the Al low-Miller index surfaces made using the ASE here, make 100, 110 and 111 ZrC surfaces, and compute the surface formation energies.  

#### ZrC Wulff construction
- Consider the ratio between (100), (110) and (111) surface formation energies to understand which facets are present in the Wulff construction.  (Hint: how far are each of the surface from the centre the cell.)
