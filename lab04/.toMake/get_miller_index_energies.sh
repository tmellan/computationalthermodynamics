#!/bin/bash
for i in $(echo 01_al_unitcell *_1*_*); do 
	ei=$(grep ! $i/*out | tail -n 1 | awk '{print $5}') 
	echo $i $ei 
done | column -t > surface_energy.dat 

cat surface_energy.dat
