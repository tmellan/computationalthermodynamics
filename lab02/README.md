Lab 2: DFT calculations with Quantum Espresso
===============================

[Back to Course Overview](..)

Objectives
----------------------
The focus of this session is to practise running DFT jobs with quantum-espresso
and learn how to calculate a number of important crystal properties. For the sake of speed 
we will work with perfect bulk systems, which have high symmetry, and are faster to converge. The techniques form the basis of DFT calculations we will do for defective crystal systems in the next session. 

Aims:
- Understand important QE input and output.
- Converge essential calculation parameters such as planewaves and k-point sampling.
- Learn how to compute interatomic forces, and determine lattice parameters by minimising the crystal total energy.
- Calculate electron eigenvalues and free energy. 
- Calculate phonon density of states and free energy. 


## 1. QE IO
-----------

In the previous lesson we performed a DFT calculation with Quantum Espresso using a pre-prepared input file for aluminium. Here we will create our own Quantum Espresso input file, run the DFT calculation, and examine the output in more detail. 

### Input
Navigate to the directory `01_diamond`, make a file called `C_diamond.in`, and open it to edit with a text editor such as `gedit`, `nano` or `vi`:
- `cd ~/computationalthermodynamics/lab01/`
- `pwd`
- `mkdir 01_diamond`
- `cd 01_diamiond`
- `pwd`
- `touch C_diamond.in`
- `ls -alrth` 
- `vi C_diamond.in`

In the input file, first three sections we need are `CONTROL`, `SYSTEM`, and `ELECTRONS`. 
Fill the `CONTROL`  section in input file `C_diamond.in` with the following text:
```
&CONTROL
   pseudo_dir = '.'         ! Pseudopotential directory 
   calculation = 'scf'      ! Self-consistent field calculation
   etot_conv_thr = 1.0D-4   ! Default in Rydberg.
/
```
`pseudo_dir = '.'` specifies that the pseudopotentials are located in the same directory thatthe QE DFT calculation will run in. Alternatively we could specify some specific absolute path, `pseudo_dir = '/user/home/path/to/potentials/'`, or a relative path like one up `pseudo_dir = '../.'`. `calculation = 'scf'` means we will do a self-consistent field (SCF) DFT calculation -- guess the wavefunction, work out the electron density, calculate potential, solve Kohn-Sham equations, repeat until the energy tolerance threshold is satisfied, in this instance `etot_conv_thr = 1.0d-4`. To see what other `CONTROL` keywords can be specified, check out the QE [docs online](https://www.quantum-espresso.org/Doc/INPUT_PW.html).

The `&SYSTEM` block defines our crystal system and planewave cutoff:
```
&SYSTEM
   ibrav = 2      ! Bravais Lattice index -- 2 for fcc
   A = 3.57       ! Lattice parameter in Ang
   nat = 2        ! Num of atoms in cell
   ntyp = 1       ! Num of atom types
   ecutwfc = 40.0 ! Cutoff energy in Rydberg
/
```
Diamond has a cubic structure with two interpenetrating fcc lattices. For an fcc Bravais-lattice, quantum espresso uses the keyword `ibrav=2` (see Quantum Espresso [documentation](https://www.quantum-espresso.org/Doc/INPUT_PW.html)). The number of atoms in the Bravais lattice is `nat=2`, number of atom types `ntyp=1`, and the diamond lattice parameter we specify is 3.57 Angstrom.

In this section we also set the planewave cutoff energy `ecutwfc`. A minimum value for `ecutwfc` is stated in each pseudopotential file. For the carbon pseudopotential we will use the value of 40 Ry. In production level calculations, the cutoff energy is carefully converged. The choice of cutoff is a trade between increased accuracy at the expense of increased computational resource timee. Note, on the choice of cutoff, comparing calculations with different cutoff energies is not generally meaningful (except if the different calculations are carefully referenced), so once a cutoff is chosen it is fixed for all jobs for which energy differences will be considered.

Parameters for electronic convergence are set next in the `&ELECTRONS` block:
```
&ELECTRONS
   conv_thr = 1.0d-8 ! convergence threshold on the total energy in a.u.
   mixing_beta = 0.7 ! mixing factor for self-consistency
/
```
Though we are mostly accepting defaults, the parameters shown are being highlighted because these are ones it is often useful to play with to improve accuracy or efficiency. For the full list of options see the [docs](https://www.quantum-espresso.org/Doc/INPUT_PW.html).

Finally define the atomic species, position of atoms in the crystal, and the k-point mesh that specifies the reciprocal-space sampling of the electronic band structure:
```
ATOMIC_SPECIES
 C 12.011 C.pbe-n-kjpaw_psl.1.0.0.UPF ! atom type, mass, pseudopotential

ATOMIC_POSITIONS crystal
 C 0.000 0.000 0.000   ! first atom: type, fractional xyz coordinate
 C 0.250 0.250 0.250   ! second atom: type, fractional xyz coordinate

K_POINTS automatic
 4 4 4 1 1 1           ! Automatically generated regular 4x4x4 grid along crystal axes. 
                       ! xyz offsets by half grid step switched on by 1 1 1 
```
In this section we have asked to use the carbon `C.pbe-n-kjpaw_psl.1.0.0.UPF` pseudopotential, which is a PBE PAW one. Different pseudopotentials are available [here](https://www.quantum-espresso.org/resources/faq/pseudopotentials). For a given set of calculations, in general the potential for a given species should not be changed, as different potentials have different absolute energies for the same element.

The lattice parameter and atomic positions of carbon in fcc diamond are well known. We can check they are indeed reasonable by visualising the crystal structure with the command:
```
xcrysden --pwi C_diamond.in
```

As long as the structure is relatively common, the basic crystal structure can probably be found in a Xray crystallography publication or picked up from an online database such as [ICSD](https://icsd.fiz-karlsruhe.de/index.xhtml), [COD](http://www.crystallography.net/cod/index.php), or [The Materials Project](https://materialsproject.org/). These databses aggregate crystal structure info from experimental and theoretical studies, and usually provide the structure as a `.cif` crystallographic information file. As well as using [xcrysden](http://www.xcrysden.org/) to inspect the structure, [VESTA](https://jp-minerals.org/vesta/en/) is another excellent tool, particularly for making publication quality images. VESTA supports many crystal file formats, so can be used to export a `cif` crystallographic information file from a crystal databse, to atomic positions in the form of cartesian xyz coordinates for a quantum-espresso input file.

#### _Task_
1) Browse the [docs](https://www.quantum-espresso.org/Doc/INPUT_PW.html) to see if we should change default values for any other calculation parameters. 
2) Download a diamond `.cif` file from [COD](http://www.crystallography.net/cod/index.php). Examine the structure with VESTA and export the xyz coordinates. Are they the same as in our input? If not why?

### Output
In this section we will examine the output made by quantum-espresso.
Run a DFT calculation for our C_diamond input file, using the `pw.x` executable, in this instance printing the output to screen:
```
pw.x < C_diamond.in
```
Inspect the screen output and any other files produced. 

A typical calculation may take hours or days to complete, and produce a lot of output, so printing to screen is not desirable. Redirect the standard output to file using the command::
```
pw.x < C_diamond.in & > C_diamond.out
```
In general this is how we will handle input and output from the quantum-espresso `pw.x` code. If the code takes longer than a few seconds, it is a good idea to let it run in the background by appending our command with `&`.

Find the output energy by searching with `less`, `cat`ting the output file (not a good idea
if the output is large) or by scrolling through with a text editor. 

You can also quickly find the total energy by grepping the character '!' which in on the total energy line as:
```
grep ! *.out
!    total energy              =     -22.07161109 Ry
```

#### _Task_ 
1) Copy the directory 01_diamond to a new directory 02_diamond. In this one rerun the calculation after changing the lattice parameter by +1%. Is the energy of the expanded structure higher or lower?

2) Create a new directory 03_graphite. Make suitable changes and compute the energy for the graphite structure by analogy with the diamond calculation. Does this agree with your expectation of which carbon polymorph is more
stable? How could we more accurately compute which phase is more stable?

##### _Tip_ 
Use pattern matching and globbing to simplify checking the total energy from each calculation. For example:
```
grep !  0{1..3}_*/*.out | column -t
01_diamond/C_diamond.out:!           total  energy  =  -36...  Ry
02_diamond_expanded/C_diamond.out:!  total  energy  =  -36...  Ry
03_graphite/C_graphite.out:!         total  energy  =  -73...  Ry
```
This is much faster and neater than typing `grep !  01_C_diamond/C_diamond.out` for each file individually,
or opening each file with a text editor and searching for the total energy by hand. 
To see how the `{1..3}` brace expansion and `*` wildcard matches the output to the files we want, type `echo 0{1..3}_*/*out`.

## 2. Parameter convergence
---------------------------

Practical electronic structure DFT calculations rely on parameters that determine how well quantities of interest like total energies are 'resolved'. As bare minimum the parameters for
kinetic energy cutoff (number of planewaves), and k-point sampling of the electronic band structure,
should be converged. Converged parameter values may be reported in the literature for your system of interest,
but it is a good habit to converge the parameters yourself, and may save you a lot of time in the long run.

### Cutoff energy convergence

Make a directory `04_diamond_ecut_convergence` containing a template diamond input file `C_diamond.in` filled with text like:
```
&CONTROL
   pseudo_dir = '.'
/

&SYSTEM
   ibrav = 2      ! Bravais Lattice index -- 2 for fcc
   A = 3.57       ! Lattice parameter in Ang
   nat = 2        ! Num of atoms in cell
   ntyp = 1       ! Num of atom types
   ecutwfc = xxCUTxx.0 ! Cutoff energy in Rydberg
/

&ELECTRONS
   conv_thr = 1.0d-8 ! SCF convergence
   mixing_beta = 0.7 ! mixing factor for self-consistency
/

ATOMIC_SPECIES
 C 12.011 C.pbe-n-kjpaw_psl.1.0.0.UPF

ATOMIC_POSITIONS crystal
 C 0.000 0.000 0.000
 C 0.250 0.250 0.250

K_POINTS automatic
 4 4 4 1 1 1
```
Note, `ecutwfc` is set to `xxCUTxx.0`. This is to give us something to `grep` onto, so we can setup
a series of calculations automatically using a bash script. To setup the input files,
make a file called `setup_ecut_convergence_input_files.sh` and fill it with the text:
```
#!/bin/bash

for testCut in {15..50..5};
do
    sed -e 's/xxCUTxx/'$testCut'/g' C_diamond.in > "C_diamond_"$testCut".in"
done
```
Make the file executable, `chmod 755 setup_ecut_convergence_input_files.sh`, and run the script, 
`./setup_ecut_convergence_input_files.sh`. Inspect that the input files are as expected,
for example with `ls -alrth` then `head -n 20 C_diamond_*.in`.

Now make another script to run the jobs, making a file called `run_ecut_convergence_test.sh`
filled with the text:
```
#!/bin/bash

for testCut in {15..50..5}; 
do
    pw.x <  "C_diamond_"$testCut".in"  > "C_diamond_"$testCut".out"
done
```

Run the script and when the jobs have completed, create a file containing cutoff energy vs total energy. 
You can get this data with the command 
`awk '/kinetic-energy/{ecut=$4} /!/{print ecut, $5}' *out > cutoff_totalenergy.dat`.

Inspect the output data, for example, `cat cutoff_totalenergy.dat`, then plot it in the command line.
Type `gnuplot`, to open the interactive plotting shell, then
```
gnuplot> p "cutoff_totalenergy.dat" u 1:2 w l, "" u 1:2
gnuplot> set xlabel "Cutoff energy (Ry) "
gnuplot> set ylabel "Total energy (Ry)"
gnuplot> unset key
gnuplot> p "cutoff_totalenergy.dat" u 1:2 w l, "" u 1:2
gnuplot> set xrange [20:]
gnuplot> p "cutoff_totalenergy.dat" u 1:2 w l, "" u 1:2
gnuplot> set xrange [40:]
gnuplot> p "cutoff_totalenergy.dat" u 1:2 w l, "" u 1:2
```

![e_cut_convergence](.images/e_cut_convergence.png)

### k-point convergence

Converge k-point sampling for graphite in a directory `05_graphite_kpoint_convergence`. You can start with an input template file like:
```
&CONTROL
   pseudo_dir = '.'
/

&SYSTEM
   ibrav = 4      ! Bravais Lattice index -- 4 for Hexagonal
   A = 2.46       ! A lattice parameter in Ang
   C = 6.70       ! A lattice parameter in Ang
   nat = 4        ! Num of atoms in cell
   ntyp = 1       ! Num of atom types
   ecutwfc = 40.0 ! Cutoff energy in Rydberg
/

&ELECTRONS
   conv_thr = 1.0d-8
/


ATOMIC_SPECIES
 C  12.011  C.pbe-n-kjpaw_psl.1.0.0.UPF

ATOMIC_POSITIONS crystal
 C 0 0 1/4
 C 0 0 3/4
 C 1/3 2/3 1/4
 C 2/3 1/3 3/4


K_POINTS automatic
  xxKPxx xxKPxx 4 1 1 1
```
The k-point grid parameter we will vary is xxKPxx (and yyKPyy by symmetry). Make the k-point convergence input files with a script like:
```
#!/bin/bash
templateName="C_graphite"
for kpx in {2..9}; do
    sed -e 's/xxKPxx/'$kpx'/g' $templateName".in" > $templateName"_"$kpx".in"
    pw.x < $templateName"_"$kpx".in" > $templateName"_"$kpx".out" &
done ; wait
awk '/number of k points/{nkpt=$5} /^!.*total/{print nkpt, $5}' *out > kp_energy.dat
```

#### _Task_
Determine what density of k-points is sufficient to match the energy convergence we get from using a k-point cutoff of 40 Ry (the minimum suggested value). How do you think this would compare to aluminium or diamond?

## 3. Forces and total energies
-------------------------------

### Forces
Copy the graphite calculation `03_graphite` to `06_graphite_forces`. By default quantum-espresso will not
print interatomic forces in an SCF calculation. Ask quantum-espresso to print 
the forces by adding `tprnfor=.true.` to  the control section. Run the calculation and check the interatomic 
forces match your expectations.

#### _Task_ (optional)
Calculate restoring force vs displacement by moving a single atom up to 0.15 Angstrom (e.g. in increments of 0.03 Angstrom) from the perfect equilibrium site. Compare linear and cubic fits using `gnuplot`.
Estimate the thermal displacement in z at 300 K using a linear approximation to the restoring force on the single displaced atom.

### Total energy vs lattice parameter
Determine the lattice constant of aluminium by considering how the energy changes as the crystal is expanded. 
In a directory `07_al_lattice_constant`, make a template input file `1_al_template.in`, filled with the text:
```
&CONTROL
   pseudo_dir = '.'
   calculation = 'scf'
/

&SYSTEM
   ibrav = 2      ! Bravais Lattice index -- 2 for fcc
   A = xxALATxx      ! Lattice parameter in Ang
   nat = 1        ! Num of atoms in cell
   ntyp = 1      ! Num of atom types
   ecutwfc = 29.0 ! Cutoff energy in Rydberg
   occupations='smearing'! Smearing occupation for metals
   degauss = 1.0d-01     ! Smearing width in Ry
   smearing='mp'         ! Methfessel-Paxton smearing
/

&ELECTRONS
   conv_thr = 1.0d-8  ! SCF convergence in Ry
   mixing_beta = 0.7  ! SCF mixing factor
/

ATOMIC_SPECIES
Al  26.982 Al.pbe-nl-kjpaw_psl.1.0.0.UPF

ATOMIC_POSITIONS crystal
Al 0.000000000 0.000000000 0.000000000

K_POINTS automatic
5 5 5 1 1 1
```
Set up and run the DFT total energy calculations at different lattice parameters by making a script like below. That is,  opening a text file in the same directory as our template input file (above), putting in the following text, saving the text file, making the text file excutable, the running the script. 

Note please maintain a sensibile directory structure. For example, run this calculation in a new directory rather than running all calculations in this lab in the same directory! This will prevent calculations getting mixed up. 
```
#!/bin/bash

#Lattice constants to test
latticeParameterList="3.7 3.8 3.9 4.0 4.1 4.2 4.3 4.4 4.5 4.6 4.7"

#For loop to setup qe input file, and run each qe calculation
for testValue in $latticeParameterList ; do
    sed -e 's/xxALATxx/'$testValue'/g' 1_al_template.in > "1_al_"$testValue".in"
    pw.x < "1_al_"$testValue".in" > "1_al_"$testValue".out" &
    wait
done
wait

#Collect results
echo "a(Ang) energy(Ry)" > aLatt_energy.dat
for testValue in $latticeParameterList ; do
        e=$(grep ! "1_al_"$testValue".out" | awk '{print $5}')
        echo $testValue $e  >> aLatt_energy.dat
done
```

#### _Task_ (optional)
- Fit a 3rd order polynomial to the data and plot the fitted polynomial using gnuplot:
```
gnuplot> f(x)=a+b*x+c*x**2+d*x**3
gnuplot>  a=1; b=1; c=1; d=1
gnuplot> fit f(x) "aLatt_energy.dat" via a, b, c, d
gnuplot> set title "Energy vs lattice constant"
gnuplot> set xlabel "Lattice constant (Ang)"
gnuplot> set ylabel "Energy (Ry)"
gnuplot> p "< tail -n +2 aLatt_energy.dat" u 1:2 t "data points", f(x) t "3rd order approximation"
gnuplot> set term png
gnuplot> set output "al_lattice_parameter_energy_variation.png"
gnuplot> p "< tail -n +2 aLatt_energy.dat" u 1:2 t "data points", f(x) t "3rd order approximation"
```
- Find the lattice constant by minimising the fitted function using a method of your choice (since it is a 3rd order you could do it by hand -- next we will see how this can be easily automated using a few python commands).

### Find the Al lattice constant using python

Open a python interactive notebook by typing `jupyter notebook`. 

Import python libraries for `matplotlib` for plotting, `numpy` for working with arrays, and `scipy` for fitting and optimisation
```
import numpy as np
import scipy.optimize as so
import matplotlib.pyplot as plt
```

Set the directory to import the data from and load the data. For me this looks like the following, but for you change the path appropriately.
```
dir="/Users/thomasmellan/Documents/teaching/computationalthermodynamics/lab02/.toMake/07_al_lattice_constant/"
alat_u0=np.loadtxt(dir+"aLatt_energy_noheader.dat")
alat_u0
```
Here `aLatt_energy_noheader.dat` is the lattice parameter vs energy file produced in the previous section, without the 'a(Ang) energy(Ry)' header. You can make it yourself or find it [here](https://gitlab.com/tmellan/computationalthermodynamics/blob/master/lab02/.toMake/07_al_lattice_constant/aLatt_energy_noheader.dat). 

The imported data is a 2D aLatt-energy array. Separate it into two 1D arrays, and plot:
```
alat, u0 = alat_u0[:,0], alat_u0[:,1]
print(alat, u0)

plt.scatter(alat, u0)
plt.xlabel("a (Å)")
plt.ylabel("Energy (Ry)")
plt.show()
```

To find the minimum energy lattice parameter, we will fit an nth-order power series to the data. This is a function we can  easily differentiate in python, and therefore find the optimum lattice parameter.
Here is a function that fits nth order polynomials to the data. Then we plot the fits along with the original data.
```
def polynomial_fit(x, y, order,printing=False):
    coefs = np.polyfit(x, y, order)
    fit = np.poly1d(coefs)
    if printing == True:
        print(fit)
        print(coefs)
    return fit

u0_volume_al_fit1 = polynomial_fit(alat, u0, 1)
u0_volume_al_fit2 = polynomial_fit(alat, u0, 2)
u0_volume_al_fit3 = polynomial_fit(alat, u0, 3)
u0_volume_al_fit4 = polynomial_fit(alat, u0, 4,True)

fig=plt.figure(figsize=(15, 10))
plt.scatter(alat, u0)
plt.plot(np.linspace(3.7, 4.7), u0_volume_al_fit1(np.linspace(3.7, 4.7)))
plt.plot(np.linspace(3.7, 4.7), u0_volume_al_fit2(np.linspace(3.7, 4.7)))
plt.plot(np.linspace(3.7, 4.7), u0_volume_al_fit3(np.linspace(3.7, 4.7)))
plt.plot(np.linspace(3.7, 4.7), u0_volume_al_fit4(np.linspace(3.7, 4.7)))
plt.xlabel("Volume (Å^3)")
plt.ylabel("Total Energy (eV/Å^3)")
plt.show()
```

Use the scipy.optimize routine to find the mininmum of the fit
```
print(so.minimize(u0_volume_al_fit4,x0=4.3))
#The minimum occurs at alat = 
min4th.x
```

Make a function to find the optimum lattice parameter for different fit orders
```
def find_alatt(x, y, order):
    fit = polynomial_fit(x, y, order)
    alat=so.minimize(fit,x0=4.3)
    return np.array([order, alat.x[0]])

order_alat=np.array([find_alatt(alat, u0, order) for order in range(2,6)]).T
print(order_alat)
```

Plot lattice parameter vs fit order.
```
plt.plot(*order_alat)
plt.xlabel("Fit order")
plt.ylabel("a (Ang)")
plt.show()
```

Check the output of your python code against [this jupyter notebook](.toMake/07_al_lattice_constant/al_lattice_parameter.pdf) on finding the optimised aluminium lattice constant.

## 4. Electronic bandstructure and electronic free energy
--------------------------------------------------------

### Diamond band structure (optional)

To compute the electronic band structure of diamond we will follow three steps. 
1. SCF calculation to generate a charge density.
2. NSCF continuation calculation, sampling k-points along high symmetry directions.
3. Run the `bands.x` executable to generate the band structure.

- For the first step, make an input file called `01_C_diamond_scf.in`, in a directory `08_diamond_bandstructure`, 
containing the text:
```
&CONTROL
   pseudo_dir = '.'
   calculation = 'scf'
/

&SYSTEM
   ibrav = 2      ! Bravais Lattice index -- 2 for fcc
   A = 3.57       ! Lattice parameter in Ang
   nat = 2        ! Num of atoms in cell
   ntyp = 1       ! Num of atom types
   ecutwfc = 40.0 ! Cutoff energy in Rydberg
/

&ELECTRONS 
/

ATOMIC_SPECIES
 C  12.011  C.pbe-n-kjpaw_psl.1.0.0.UPF

ATOMIC_POSITIONS crystal
 C 0.00 0.00 0.00
 C 0.25 0.25 0.25

K_POINTS automatic
  12 12 12 1 1 1
```

- In the same directory, for the NSCF calculation, make an input file `02_C_diamond_band1.in` containing:
```
 &CONTROL
    pseudo_dir = '.'
    calculation = 'bands'
 /

 &SYSTEM
    ibrav =  2
    A = 3.57
    nat =  2
    ntyp = 1
    ecutwfc = 40.0
    nbnd = 12 ! Add 4 extra conduction bands also 
 /

 &ELECTRONS
 /

ATOMIC_SPECIES
 C  12.011  C.pbe-n-kjpaw_psl.1.0.0.UPF

ATOMIC_POSITIONS crystal
 C 0.00 0.00 0.00
 C 0.25 0.25 0.25

# Path here goes: Γ K X Γ' L X W L
K_POINTS crystal_b
  8
  0.000 0.000 0.000 30
  0.375 0.375 0.750 10
  0.500 0.500 1.000 30
  1.000 1.000 1.000 30
  0.500 0.500 0.500 30
  0.000 0.500 0.500 30
  0.250 0.500 0.750 30
  0.500 0.500 0.500 0
```
- Finally make a `03_bands.in` file accepting default parameters:
```
 &BANDS
 /
```
Now run the jobs with a script `run.sh` like:
```
#!/bin/bash
pw.x < 01_C_diamond_scf.in > 01_C_diamond_scf.out 
pw.x < 02_C_diamond_band1.in > 02_C_diamond_band1.out 
bands.x < 03_bands.in > 03_bands.out
```
A file called `bands.out.gnu` should have been produced. We can plot this with `gnuplot`:
```
gnuplot> set xtics ("Ã" 0.0000, "K" 1.0607,"X" 1.4142, "Ã" 2.4142, "L" 3.2802, "X" 4.1463, "W" 4.6463, "L" 5.3534)
gnuplot> set grid xtics lt -1 lw 1.0
gnuplot> unset key
gnuplot> set ylabel "Energy (eV)"
gnuplot> set title "Carbon Diamond Electronic Band Structure"
gnuplot> efermi=13.99
gnuplot> plot "bands.out.gnu" u 1:($2-efermi) with lines
```


#### Electronic free energy

In the first lab we calculated the density of states for aluminium. Take a new directory, copy the Al DOS calculations to it and make  a script and rerun the DOS calculations, increasing the k-point sampling in the NSCF calculation to 25x25x25.

In the same directory as the Al density of states calculation, we will use python to calculate electronic thermodynamics for Al. 

Some methods to calculate the electronic free energy at different levels of sophistication are given in this [paper](https://journals.aps.org/prb/pdf/10.1103/PhysRevB.95.165126), specifically:
1. Sommerfeld approximation
2. Static DOS method
3. Fermi-Dirac smearing DFT total energy method 
4. Fermi-Dirac smearing DFT total energy method with coupling to vibrations.

Here we will use the first two approximations which require less computational resources.  

To start you might find it useful to shift the density of states so the Fermi energy is zero using a bash script like, `shift_dos.sh`:
```
dos_file="pwscf.dos"                                                                #The name of the file containing the electronic DOS
efermi=$(grep EFermi $dos_file | awk '{print $9}')                                  #Find the Fermi energy
awk -v ef=$efermi '{print $1-ef, $2}' $dos_file |tail -n +2 > dos_shifted.dat       #Shift the energy scale by the Fermi and print to file
```
To calculate the electronic entropy and free energy of aluminium you can follow this python [notebook](.toMake/09_al_electronic_dos/electronic_free_energy.pdf).

## 5. Phonons in Al

Make an aluminium conventional unit cell input file, `1_al_ibrav0.in`, in directory `10_al_phonon_dos`, with the lattice parameter set to the value previously optimised:
```
&CONTROL
   pseudo_dir = '.'
   calculation = 'scf'
/

&SYSTEM
   ibrav = 0      ! Bravais Lattice index -- 0 for free (give cell parameters)
   nat = 4        ! Num of atoms in cell
   ntyp = 1      ! Num of atom types
   ecutwfc = 29.0 ! Cutoff energy in Rydberg
   occupations='smearing'! Smearing occupation for metals
   degauss = 1.0d-01     ! Smearing width in Ry
   smearing='mp'         ! Methfessel-Paxton smearing
/

&ELECTRONS
   conv_thr = 1.0d-8  ! SCF convergence in Ry
   mixing_beta = 0.7  ! SCF mixing factor
   diagonalization = 'david'
/

K_POINTS automatic
4 4 4 1 1 1

CELL_PARAMETERS (Bohr)
7.640045560880000     0.000000000000000    0.000000000000000
0.000000000000000     7.640045560880000    0.000000000000000
0.000000000000000     0.000000000000000    7.640045560880000

ATOMIC_SPECIES
 Al   26.98154   Al.pbe-nl-kjpaw_psl.1.0.0.UPF

ATOMIC_POSITIONS crystal ! fractional positions
Al 0.00 0.00 0.00
Al 0.50 0.50 0.00
Al 0.00 0.50 0.50
Al 0.50 0.00 0.50
```

Make a 2x2x2 supercell of the conventional unit cell using [phonopy](https://atztogo.github.io/phonopy/qe.html) with the command, `phonopy --qe -d --dim="2 2 2" -c 1_al_ibrav0.in`. This will produce a series of files, `supercell-{001..n}.in`, containing supercells indexed for different small displacements.  Use VESTA to check the supercell looks like:
![al_phonons](.images/al_2x2x2.png)

Make file `al_header.in` containing the parameters for all our supercell calculations:
```
&CONTROL
   pseudo_dir = '.'    ! Pseudopotentials directory (here)
   calculation = 'scf' ! Self-consistent field calculation
   tprnfor = .true.    ! calculate interatomoic forces
   tstress = .true.    ! calculate stresses
/

&SYSTEM
   ibrav = 0             ! Bravais Lattice index -- 0 for free (give cell parameters)
   nat = 32              ! Num of atoms in cell
   ntyp = 1              ! Num of atom types
   ecutwfc = 29.0        ! Cutoff energy in Rydberg
   occupations='smearing'! Smearing occupation for metals
   degauss = 1.0d-01     ! Smearing width in Ry
   smearing='mp'         ! Methfessel-Paxton smearing
/

&ELECTRONS
   conv_thr = 1.0d-9  ! SCF convergence in Ry
   mixing_beta = 0.7  ! SCF mixing factor
   diagonalization = 'david'
/

K_POINTS automatic
2 2 2 1 1 1
```
Combine the header file with the supercell structures and run the DFT calculations using a script like:
```
#!/bin/bash

#Initial setup notes
# 1) Make 1_al_ibrav0_unitcell.in unitcell input file
# -- The lattice parameter should be the relaxed one
# -- Set it in Bohr so phonopy knows how to deal with it
# 2) Make supercells with displacements: phonopy --qe -d --dim="2 2 2" -c  1_al_ibrav0_unitcell.in
# 3) Make al_header.in file containing supercell calculation information

#Documentation notes
#Examplhttps://atztogo.github.io/phonopy/qe.htmle from the phonopy documentation can be found here:
#https://atztogo.github.io/phonopy/qe.html
#Within input files here:
#https://github.com/atztogo/phonopy/blob/master/example/NaCl-pwscf/NaCl-001.in

#Set the second index in the brace expansion to the number of supercells
for i in {001..001}; do
	cat al_header.in supercell-$i.in > 2_al_ph_$i.in
	pw.x < 2_al_ph_$i.in > 2_al_ph_$i.out &
	wait
done
wait
phonopy --qe -f 2_al_ph_001.out
wait
phonopy --qe -c 1_al_ibrav0_unitcell.in -p band.conf
phonopy --qe -c 1_al_ibrav0_unitcell.in -p mesh.conf
```
After the displaced supercell DFT jobs have completed, the command `phonopy --qe -f 2_al_ph_001.out` finds the force constant matrix. The command `phonopy --qe -c 1_al_ibrav0_unitcell.in -p band.conf`  plots the phonon dispersion and 
`phonopy --qe -c 1_al_ibrav0_unitcell.in -p mesh.conf` produces and plots the density of states file `total_dos.dat`.

The `band.conf` parameter file is 
```
DIM = 2 2 2
BAND = 0.0 0.0 0.0   0.5 0.5 0.0  0.0 0.0 0.0  0.5 0.5 0.5
BAND_LABELS = $\Gamma$ X $\Gamma$ L $\Gamma$
BAND_POINTS = 101
```
and the `mesh.conf` parameter file
```
ATOM_NAME = Al
DIM =  2 2 2
MESH = 25 25 25
```

### Al phonon thermodynamics
Compute the phonon free energy and heat capacity of aluminium, using the aluminium phonon density of states produced by phonopy in the previous expample. You can do this by using a python interactive noteook by typing `jupyter notebook`, and following my [phonon_free_energy](.toMake/10_al_phonon_dos/phonon_free_energy.pdf) notebook. It is intended that you use the phonon density of states you have calculated yourself, but if anything goes wrong, [here](https://gitlab.com/tmellan/computationalthermodynamics/blob/master/lab02/.toMake/10_al_phonon_dos/total_dos.dat) is a backup one so you can still continue with the free energy calculation.

------------------------------------------------------------------------------

Summary
-------
In this session we covered the important input parameters needed to setup
some common types of DFT calculation.  Hopefully you are now comfortable converging calculation parameters,
optimising crystal lattice constants and calculating interatomic forces. In addition we have calculated electron and phonon densities of states, and used them to work out electron and phonon thermodynamics. 

