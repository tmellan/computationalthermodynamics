dos_file="pwscf.dos"                                                                #The name of the file containing the electronic DOS   
efermi=$(grep EFermi $dos_file | awk '{print $9}')                                  #Find the Fermi energy
awk -v ef=$efermi '{print $1-ef, $2}' $dos_file |tail -n +2 > dos_shifted.dat       #Shift the energy scale by the Fermi and print to file
