set xtics ("Î" 0.0000, "K" 1.0607,"X" 1.4142, "Î" 2.4142, "L" 3.2802, "X" 4.1463, "W" 4.6463, "L" 5.3534)
set grid xtics lt -1 lw 1.0
unset key
set ylabel "Energy (eV)"
set title "Carbon Diamond Electronic Band Structure"
efermi=13.99
plot "bands.out.gnu" u 1:($2-efermi) with lines
