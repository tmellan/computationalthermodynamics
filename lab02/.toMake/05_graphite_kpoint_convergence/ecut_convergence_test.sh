#!/bin/bash
templateName="C_graphite"
for kpx in {2..9}; do
    sed -e 's/xxKPxx/'$kpx'/g' $templateName".in" > $templateName"_"$kpx".in"
    pw.x < $templateName"_"$kpx".in" > $templateName"_"$kpx".out" &
done ; wait
awk '/number of k points/{nkpt=$5} /^!.*total/{print nkpt, $5}' *out > kp_energy.dat
