#!/bin/bash

#Initial setup
# 1) Make 1_al_ibrav0_unitcell.in unitcell input file
# -- The lattice parameter should be the relaxed one
# -- Set it in Bohr so phonopy knows how to deal with it
# 2) Make supercells with displacements: phonopy --qe -d --dim="2 2 2" -c  1_al_ibrav0_unitcell.in
# 3) Make al_header.in file containing supercell calculation information

#Examplhttps://atztogo.github.io/phonopy/qe.htmle from the phonopy documentation can be found here:
#https://atztogo.github.io/phonopy/qe.html
#Within input files here:
#https://github.com/atztogo/phonopy/blob/master/example/NaCl-pwscf/NaCl-001.in

#Set the second index in the brace expansion to the number of supercells
for i in {001..001}; do 
	cat al_header.in supercell-$i.in > 2_al_ph_$i.in 
	pw.x < 2_al_ph_$i.in > 2_al_ph_$i.out &
	wait
done
wait
phonopy --qe -f 2_al_ph_001.out
wait
phonopy --qe -c 1_al_ibrav0_unitcell.in -p band.conf
phonopy --qe -c 1_al_ibrav0_unitcell.in -p mesh.conf
