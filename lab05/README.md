Lab 5: Final session
===============================

[Back to Course Overview](..)

Objectives
----------
In the final session, until the short course closes in the afternoon, we will concentrate on:
- Uncompleted lab session problems e.g. on thermal contributions to the vacancy formation free energy of aluminium, or how to compute low Miller index surface properties of ZrC. In particular please try to go through each of the jupyter notebooks.
- For the second half we can consider DFT calculations and theory in your own research.


------------------------------


After the course, if you want to examine any quantum-espresso files or python jupyter notebooks (.ipynb files) for any lab section, download the whole respository as `git clone https://gitlab.com/tmellan/computationalthermodynamics.git`. 
Then if you want to look at lab04 calculations, go to `cd computationalthermodynamics/lab04/.toMake/.` and `ls` to see the calculations directories for lab04. 
The DFT calculation input and output files for each of the lab sessions are located in the `.toMake` subdirectory of `lab0x` for x in {1..4}. 

Our calculations this week were aimed to give a starting point on how to setup and run DFT jobs, and analyse the output of your defect calculations. The calculations are not publication standard as we have used rough sampling parameters in order to be able to run the jobs quickly on your laptop computers. For production level calculations, k-point sampling grid, cutoff energy, smearing width, and supercell size, must be converged for any quantities of interest, such as defect formation energies.

The calculations here invevitably contain a number errors -- thanks for pointing out the ones found so far! If you notice any more, please let me know and I'll fix them.

If there is any research or technical question you would like to discuss after today, you are most welcome to contact me, `t.mellan` at `imperial.ac.uk`.

