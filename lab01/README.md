Lab 1: Intro to the Linux Shell, Scripting, and Plotting
===============================

[Back to Course Overview](..)

Objectives
----------------------
- Use the command line to navigate the directory structure, manipulate files and directories.
- Cover useful terminal commands and learn how to use funtionality like wildards and
  brace expansions.
- Learn how to automate command line tasks with for loops, and how to control jobs.
- Learn how plot in the command line using gnuplot.
- Run a DFT calculation for the electronic density of state (DOS) of Al, determine the Fermi level and DOS at the Fermi level, and plot the DOS vs energy.


The command line
----------------------

PC users will be familiar with the graphical-user interface (GUI) to interact with their machine, e.g., move files or execute 
code by clicking through dialogue boxes with a mouse.
Here  we focus on the command-line interface (CLI), which is a text-typing interface inside a terminal window. 
Initially this may seem less intuitive, even old fashioned, but the effective use of the command line is important to
effectivelty do first principles type calcultions for several reasons:
- repetitive tasks can be easily automated in a script
- it gives fine-grained control and more flexibility over how we use our machine
- in general it is faster than a GUI for most simple tasks
- and practically, the command line is often the only way to interact with servers in high-performance computing facilities, which  are common for large-scale _ab initio_ simulation to compute the properties of defects in crystals.

The downside is you need to know what commands to type. The purpose of this section is to get everyone up to speed on
essential command line usage, to be in a good position to do some interesting calculations.

Navigating Files and Directories
---------------------

Once you have opened the terminal, the first thing you need to be able to do
is navigate the file structure.

### pwd
To orient yourself, type `pwd`. This shows the shows the folder (directory) you are in. 
The output of `pwd` is the full pathname of the current working directory, which for me is  `/home/tmellan/Documents/teaching/computationalthermodynamics/lab01/`. 
This shows I'm in the 'lab01' folder, which is a sub-directory of 'computationalthermodynamics', which is in turn a sub-directory of 'teaching', etc..

Why is the command named `pwd`?  The letters `pwd` stand for `p`rint `w`orking `d`irectory. The slightly cryptic name is probably an artefact of the value placed on brevity to interact effectively with the unix shell 45 years ago. 

### ls

`ls` is the command used to list the contents of a directory. Contents can be files and other directories.

Examples to try:
- type `ls .`. This command means list the files our current directory, where
`.` means 'here'. 
- We can look at the contents of a specific folder by typing the full pathname
as an argument, such as `ls /home/tmellan/Documents/`. 
- To examine the files one level up in the file structure, type `ls ../`. Specifying a relative path like `../` 
is useful and often used. If we are in `/home/tmellan/Documents` it shows the files in `/Users/thomas/`.
- If we drop the arguments and type `ls`, this defaults to `ls .` (list files here).

#### Useful Options
- `ls -l` shows the files in `l`ong format with file size and permissions.
- `ls -lh` makes the file sizes easy to read for `h`umans. Most humans find 27 kB easier to undestand than 27315 Bytes.
- `ls -a` lists `a`ll files including hidden ones. Hidden files start with a dot like `.config` (and are normally hidden because it is important not to delete them by accident).
- `ls -t` sorts the files by `t`ime of creation. This is useful is you have many files in a directory.
- `ls -tr` by `r`everse time order. Often we want to see the most recent file first rather than the oldest one.
- You can combine several options like `ls -lthr`.

#### Tip -- aliases
The command `ls -lthr` is quite useful. To avoid having to type out all the characters each time, we can make a shortcut or  alias
to the command. Try `alias la="ls -lthr"`. Now instead of typing `ls -lthr` you can type `la` instead.  

#### _Task_

- List the contents of a directory 'two levels up' by specifying a relative path argument to `ls`
- Examine the `ls` option flags by typing `ls --help`. Now examine contents of a directory 'three levels up', with reverse time ordering, human readable sizes, and one other flag from the `ls --help` options.

### cd

`cd` is used to change directory. You can specify either the full path, or the directory you
want to go to as a relative path, in relation to your current working directory. 

Examples:
- From `/home/tmellan/Documents/teaching/computationalthermodynamics`, move the `lab01` directory
  by typing `cd /home/tmellan/Documents/teaching/computationalthermodynamics/lab01`
- Move back to the original directory by typing `cd /home/tmellan/Documents/teaching/computationalthermodynamics`
- Typing full paths is a hassle. If we are in `/home/tmellan/Documents/teaching/computationalthermodynamics`, we
  can also move to `lab01` by typing the relative path `cd lab01`. 
- Move back to the directory 'one up' by typing `cd ../`.

#### Tip -- tab completion
After typing a command, press the tab button for suggestions for the argument. Using tab complete in the example above is faster than manually typing each letter. From your home
directory, try typing `cd De` and pressing tab. It should automatically fill
in the full command to be `cd Desktop/`.

Note, if tab completion is not working on your machine enable it by putting the following lines in your the file `~/.bashrc`:
```
# enable bash completion in interactive shells
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi
```
then typing `source ~/.bashrc`.

#### _Task_

- Find out where you are and subdirectories contained within your current directory using `pwd` and `ls`. 
  Move to a subdirectory using a relative path. 
- `~` is shorthand for the 'home' directory. Change directory to the home directory, `~`. Inspect the location and
  contents. Navigate back to `/home/tmellan/Documents/teaching/computationalthermodynamics/lab01`.

File and Directory Manipulation
------------------------------------------------------------------------------

### mkdir

`mkdir` is used to create new directories. For example try:
- `mkdir diamond` to make a directory called 'diamond'.
- Type `ls -lh`, or `la` if you have set the alias, to check it was created. 

### rm

`rm` is used to delete files. For example:
- `touch test-file` makes a file called 'test-file'.
- type `ls -lh` to examine the new file.
- Type `rm test-file` to delete, and `ls -lh` to check it was deleted.


#### Deleting directores (_danger_)

`rm` alone will delete files but not folders. To delete a folder, use
`rm -r` to delete recursively. However be careful with `rm -r`, because if you delete a root directory containing
the rest of your filesystem, it will be gone. Using `rm -r` to delete everything
is a oneway street  -- there is no recycle bin in the linux shell, so best used with caution.

Try typing:
- `mkdir temporary-directory`.
- `ls -lh` or `la` to examine it.
- `rm -r temporary-directory` to delete and `ls -lh` to check.

### cp

`cp` is used to copy files, as `cp sourcefile newfile`.

For example:
- make a file with `touch important-file`.
- copy it, `cp important-file another-important-file`.

- To copy a file another location, specify the relative path like 
  `cp important-file some-relative-path/another-important-file` or 
  `cp important-file ../another-important-file`. Note these two commands can be shortened to
  `cp important-file some-relative-path/.` and `cp important-file ../.`.

- To copy a folder, use the recursive option, `cp -r sourcedirectory newdirectory`.

### mv

`mv` is used to move files or directories from one location to another. It is also useful
to rename files and folders. 

Try moving location by:
- typing `mv another-important-file ../.` to move the file one level up. 
- typing `mv ../another-important-file .` to move it back.

#### _Task_
- Make a directory called `dft_calculations`. 
- Copy the file `important-file` to the dft_calculations directory using the `cp` command.
- Change directory `dft_calculations` and inspect the contents with `ls`.
- Change the name of the file `important-file` to `dft-input-file` using the `mv` command.

------------------------------------------------------------------------------

Reading a file
--------------

There are several built-in programs that will allow you to examine a text file
in the terminal.

### cat

`cat` is the simplest command to check the output of a file.
- `cat filename` will output the contents of `filename` to the terminal. For a
  large file you may need to scroll back up in the terminal (e.g.
  `shift+PdgUp`).

### head

`head` gives you a sneak preview of the first 10 lines of a file. To examine the first 
n lines try for example `head -n 25 file` or `head -n 1 file`.

### tail 

`tail`, as with head, but prints the end rather than the start of the file. 
- Useful tip. Often a code might be printing to file for some long period of time. To monitor
    progress use `tail -f file` to get a continuous printout of the last 10 lines. This is useful 
to monitor how the printout from a DFT calculation is progressing.


### more, and less

`more` works in a somewhat similar way to `cat`, except for files bigger than
the terminal window, it allows you to look through a page (screen) full of
data at a time.

- `more filename` to start paging through a file.
- To move to the next page press `space` or `z`.
- To move down one line of text press `enter`.
- To quit before reaching the end of the file press `q`.

`Less` is a more advanced version of `more`. 

------------------------------------------------------------------------------

### _Task_

1. Download the QE input txt [file](https://gitlab.com/tmellan/computationalthermodynamics/blob/master/q-e-doc-6.3/doc-6.3/INPUT_DOS.txt).
Examine this file using
- `cat`
- `less`
- `head`
- `tail`

------------------------------------------------------------------------------

Other Useful Command Line Features
----------------------------------


All the commands
----------------

We've covered some important commands but there are many more. Type
`compgen -c` to generate 'completion matches' for all commands. With time 
you can become familiar with many of these, although some are used
rarely and are likely to remain unknown to all but the most dedicated 
command line users.

`Builtin`s are subset of commands that are Internal and form part of the 
shell itself. 
They should be available at minimal shell in the case of a system crash, 
and are faster than External commands as they are executed from RAM.

Type `compgen -b` to generate a list of the Names of builtins for your shell. 
Pick one and type `help COMMANDNAME` to see what it does and get basic usage 
info.

For more detailed information on how to use any linux command, access the
manual with the `man` command.

- Type `man ls` to see the manual page for the ls command. 
- Type `man less` to see the manual page for the less command. 
- Type `man intro` to get an introduction to user commands.

To exit `man` pages, type `q`. The options to navigate the manual should be
the same as those used to naviaget pages witht the `less` command.

#### _Task_
- Learn a new command from `compgen -c` and think of scenario where it could be useful.

Wildcards, brace expansion and sequences
---------

Wildcards provide a way to glob or match patterns. 

- `*` matches to any characters. Example: `ls *.dat` will list all file in 
    directory ending with .dat, and `ls *` will list all files. This wildcard 
    is very common and useful, so worth remembering.
- `?` matches any single character. Example: `ls test?.dat` matches files test1.dat,
    test2.dat, test7.dat, but not test.dat or test12.dat. 
- `[abc123]` matches one character in the bracket. Example: `ls test[12].dat`
     matches file test1.dat and test2.dat but not test3.dat.
- `[a-z]` matches one character in the bracketed range. Example: `ls test_[a-y].dat`
    matches file test_a.dat, test_b.dat, test_y.dat but not test_z.dat.

Brace expansions are useful way to generate regular expressions. Try typing:
- `echo {1..12}`. 
- `echo {a..g}`.
- `echo {a..g..2}`.
- `echo a{1,2,3}b`.
- `echo *.{txt, dat}`. Note the order -- braces are expanded first, in this instance to `*.txt` and `*.dat`, then
   wildcards are matched.

Often when we want to generate a sequence the `seq` .
- `seq 1 12`
- `seq 1 3 12`. As with the last example, this could be achieved wth brace expansion, 
    (`echo {1..12..3}`), however `seq` has some advances such as accepting variable 
    arguments as in the next example. 
- `ARG1=20; ARG2=9; seq ARG1 ARG2`.
- `seq -s, 1 9`. Add a seperator, in this instance ','.
- `seq -f "%03g" 1 120`. Specify output format, useful to enforce regularity.

#### _Task_
- You can use the command `touch filename` to make a file. Use this command and a brace expansion to make the following sequence:
dft-calc-16.dat dft-calc-17.dat dft-calc-18.dat dft-calc-26.dat dft-calc-27.dat dft-calc-28.dat dft-calc-36.dat dft-calc-37.dat dft-calc-38.dat dft-calc-46.dat dft-calc-47.dat dft-calc-48.dat
- Delete any file with an index between 16 and 18 using a wildcard.

awk/sed/grep 
-------------
`Awk`, `sed` and `grep` are used for pattern matching and string manipulation. There is a lot of ways to use these commands and we'll barely scratch the surface, but here are five I find myself using over and over.

- Find matches to 'someString' in a file: `grep someString file`
- Print field 2 in a file: `awk '{print $2}' file`.
- Print fields 1 and 3 in a file: `awk '{print $1, $3}' file`.
- Make an additiional field $3, which is the sum of fields $2 and $1 in a file: `awk '{print $3=$2+$1}'1 file`. This is useful dealing with column numerical data. 
- Replace instances of 'oldString' with some 'newString' in a file: `sed -e 's/oldString/newString/g' file`. Note this will print to screen. To make the changes inplace, use the -i flag instead of -e.

#### _Task_
- Try them out on the QE input txt (file)[https://gitlab.com/tmellan/computationalthermodynamics/blob/master/q-e-doc-6.3/doc-6.3/INPUT_DOS.txt].
 
IO redirection, piping, and shell grammar
--------------

IO redirection allows you to easily send the output of a command to a file, use
a file as input for a command, or use the output of one command as input to
another command. The most frequently used features are as follows:

- `command > file` redirects the output of the command to the file. 
- `command >> file` append the output of the command to the file
- `command < fileA > fileB` inputs file1 to the command, outputs to file2. 

- ` command1 | command2` directs the output of command1 to the input of command2,
    without an intermediary file. 

- `command1 ; command2` performs command1 then command2 sequentially.  
- `command1 && command2` performs command2 iff command1 exited successfully. 
- `command1 || command 2` performs command2 iff command1 failed. 
- `command &` runs the command in the background

#### _Task_
- Using pipes. The `top` command gives info on liniux processes running. Use the command with the flags `top -b -n 1` to get a single readout. Pipe the readout to `grep` and use it to select the name of some process, e.g., `python` or `tail`, etc.. Pipe this output again to `awk` to select the process id field.   

History
-------

Type the command `history` to get a record of executed commands. To minimise typing,
the following options are useful:

- `!!` will repeat the most recent command.
- `!n` will repeat the command indexed by `n` in the history list. For example
  `!100` would repeat the command at history index 100.
- `!string` will repeat the most recent command in the history starting with
  "string".

For more details see `man history`.

#### _Task_
- Examine your history, find a long command, and repeat it as `!n`. 

Editing Text Files
------------------
To edit text in the command line, I prefer the `vi` text editor. 
Another powerful and popular choice, and the main competitor of `vi`, is `emacs`.
If you are completely unfamiliar with `vi` or `emacs`, I'd suggest the `gedit` or `nano` text editors,
which  are easier to started with, though have less of the functionality that can be useful to you in the long run.
The choice of which text editor to use is totally up to you, but unless you have experience with one
already, `nano` is a sensible choice to start with. 
[Here](https://www.howtogeek.com/howto/42980/the-beginners-guide-to-nano-the-linux-command-line-text-editor/)
is a short introduction.

### vi tips (_optional_)
For anyone interested in learning to use `vi`, here is an _optional_ and very short summary 
of some of the functions.

To create a new text file with `vi`, try the following:
- type `vi test-doc`, to open a new text file called `test-doc`.
- In order to edit this file, we have to switch on `insert mode` by pressing `i`.
  In `insert mode` you should see `-- INSERT --` at the bottom of the window. Now
  try typing some text.
- To save the text, first press the `esc` button to exit `insert mode`, then 
  `:w` to save or write the file. 
- `:q` will close the file. After closing try `cat test-doc` to print the contents
  of the file to screen and check the it saved as intended.

Using Vi with large files
- `vi ~/example-potential` to open the rather large potential file. 
- Vi has different modes,  for example `command mode`, `insert mode` and `visual mode`.
  To navigate the file after opening, press `esc` to enter `command mode`. In `command mode`
 we can use the arrow keys to navigate.  To make larger movements, press `ctrl f` to page 
  forward, then try `ctrl b` to scroll pages backwards. 
- To match to a specific pattern, press `esc` to ensure in command mode, followed by
  `/Helium` to jump to the first word Helium in the potential file. 
- In `command mode`, we can also use `G` to jump to end of the file, and `1G` jumps to the start.  
  To jump to 76% way through a file, try `76%`. To jump to a
  specific line number, press `1G` to go to line 1, then `176` to go forwards 176 lines. To check we
  are indeed at line 176, type `:set nu!`, in order to show line numbers.

- To delete text, try using `visual mode` by pressing `esc ctrl v`, using the arrow
  keys to highlight the text, then press `d` to delete. 
- To delete a whole line, press `esc dd`. To undo the delete `u`. 
- To copy a line or region of text, enter visual mode `esc ctrl v`, use the arrows
  to select to text, the press `y` for copy or yank. To paste press `esc p`.
- To copy a single whole line, press `esc yy`, then `esc p`.
- Lets save (write) the file with a new name and close (q) by typing 
  `:w Helium-potential-edited` then pressing the return key.

#### _Task_
- Open the file `dft.dat` with a text editor of your choice and add materials science to the last line.
- Create a file called `editor_text_file.txt`. Open it, add some text of your choice, then save. Type 
`cat editor_text_file.txt` to confirm the file contains the intended text.

For loops
---------------------

For loops allow the automation of repeated actions. The general form of a for loop in a bash
script is a text file containing text like:
```
#!/bin/bash

for item in list
do
	action1
	action2
	...
done
```
The first line says use the bash shell. For each item in the list, the loop does action1, then action2, etc.,
until the end of the list is reached then the loop exits. To run the script, make it executable by altering 
file permission as `chmod 755 some_script.sh`

As an example, consider five files that describe different types of loop in computer science. For the example, save the following text to each filename:

- `dowhileloop.dat`: 

```
In most computer programming languages, a do while loop is a control flow statement that executes a block of code at least once, and then repeatedly executes the block, or not, depending on a given boolean condition at the end of the block.
The do while construct consists of a process symbol and a condition.
First, the code within the block is executed, and then the condition is evaluated.
If the condition is true the code within the block is executed again.
This repeats until the condition becomes false.
```

- `foreachloop.dat`: 
```For each loop (or foreach loop) is a control flow statement for traversing items in a collection.
Foreach is usually used in place of a standard for loop statement.
Unlike other for loop constructs, however, foreach loops[1] usually maintain no explicit counter: they essentially say "do this to everything in this set", rather than "do this x times".
This avoids potential off-by-one errors and makes code simpler to read.
```

- `forloop.dat`: 
```
In computer science, a for-loop (or simply for loop) is a control flow statement for specifying iteration, which allows code to be executed repeatedly.
Various keywords are used to specify this statement: descendants of ALGOL use "for", while descendants of Fortran use "do".
There are other possibilities, for example COBOL which uses "PERFORM VARYING".
``` 

- `infiniteloop.dat`: 
```
An infinite loop (or endless loop) is a sequence of instructions in a computer program which loops endlessly, either due to the loop having no terminating condition, having one that can never be met, or one that causes the loop to start over.
 In older operating systems with cooperative multitasking, infinite loops normally caused the entire system to become unresponsive.
```

- `whileloop.dat`: 
```
In most computer programming languages, a while loop is a control flow statement that allows code to be executed repeatedly based on a given Boolean condition.
The while loop can be thought of as a repeating if statement.
```

You could inspect the first line in each as `head -n 1 dowhileloop.dat`, then 
`head -n 1 foreachloop.dat`. Alternatively we can automate the inspection by
looping over the files as
```
#!/bin/bash

for file in dowhileloop.dat foreachloop.dat forloop.dat infiniteloop.dat whileloop.dat
do
	head -n 1 $file
done
```
This example should produce the output:
```
In most computer programming languages, a do while loop is a control flow statement that executes a block of code at least once, and then repeatedly executes the block, or not, depending on a given boolean condition at the end of the block.
For each loop (or foreach loop) is a control flow statement for traversing items in a collection.
In computer science, a for-loop (or simply for loop) is a control flow statement for specifying iteration, which allows code to be executed repeatedly.
An infinite loop (or endless loop) is a sequence of instructions in a computer program which loops endlessly, either due to the loop having no terminating condition, having one that can never be met, or one that causes the loop to start over.
In most computer programming languages, a while loop is a control flow statement that allows code to be executed repeatedly based on a given Boolean condition.
```
This is example is quite contrived and could easily be accomplished with wildcard expansion. However let's make a slightly more sophisticated version. Parameterise the number of characters to print on each to N=25, 
and print some extra info indexing each printout:

```
#!/bin/bash

list_of_files=$(echo *.dat)
N="25"

counter=0
for file in $list_of_files
do
        let counter=counter+1
        echo "File" $counter", first N="$N" characters of first line:"
        head -n 1 $file | cut -c -$N
        echo ""
done
```
This script should output:
```
File 1, first N=25 characters of first line:
In most computer programm

File 2, first N=25 characters of first line:
For each loop (or foreach

File 3, first N=25 characters of first line:
In computer science, a fo

File 4, first N=25 characters of first line:
An infinite loop (or endl

File 5, first N=25 characters of first line:
In most computer programm
```

#### _Task_
Instead of setting the parameter 'N' in the script, we can pass a command line variable
to the script by setting `N=$1`. This means let `N` be the first argument after the script,
for example, like `./for_loop_example3.sh 25`.

Make a script that prints the first `N` characters of each file, where is N is a command line
argument. N this time should be 100 characters
ie `./for_loop_example3.sh 100`.  In addition, print the file name as well as the counter index.

Job Control
-----------

There a number of commands that are useful to manipulate jobs run from the terminal. 
To illustrate some job control commands, create a script prints an index and the date
every five seconds (this is a  placeholder for something more interesting we may wish to run soon):

```
#!/bin/bash
for i in {1..100}; do
	echo $i $(date)
	sleep 5
done
```

Save the script as `job_control_example_script.sh`, make it executable with 
`chmod 755 job_control_example_script.sh`, then run the script as
`./job_control_example_script.sh`. To `stop` the job, press `ctrl z`. Use 
the `jobs` command to inspect the status recently run jobs. To bring the 
last one back to life, press `fg`, which will bring the last job listed in
`jobs` back to the foreground.

Often it is useful to write to file and run the command in background, 
rather than printing to screen. Try this for our example script with 
`./job_control_example_script.sh > output.dat &`. 
Inspect the command is running with `jobs`. Check it is writing the output
as intended with `tail -f output.dat`. If we wish to terminate the job
before it ends, we can kill using the job number `kill %job_number`, or
if it is the last job, simply by `kill $!`. 

To continue running after we close a terminal window, for
example, because we are using an ssh connection, we can `disown` the job. 
Try this with `./job_control_example_script.sh > output.dat &`, followed by
`disown %job_number`. After being disowned it will no longer be listed by 
`jobs`. To terminate the job, find the PID number using `top` or `htop`.

#### _Task_

- Write a script that caputures output from `top`, selects all entries with some name (e.g. `grep 'python'`), then loops over the process IDs executing the `pwdx` command to see where the processes are being run. This type of construction is useful to find and kill jobs.

Command line plotting
---------------------

### Gnuplot

Gnuplot is a command line plotting software. It can be used interactively or as a script. 

#### Interactive plotting

Get some data to plot by saving the data in this [phonon dos file](https://gitlab.com/tmellan/computationalthermodynamics/blob/master/lab01/.toMake/plotting_example/phonon_dos_perfect_supercell.dat) as `phonon_dos_perfect_supercell.dat`. Open `gnuplot` in interactive mode by typing
`gnuplot`, then try the following commands:

```
gnuplot> p "phonon_dos_perfect_supercell.dat" u 2 
gnuplot> p "phonon_dos_perfect_supercell.dat" u 1
```
In the first command we plot column 2 of the data in `phonon_dos_perfect_supercell.dat`, 
and the second we plot column 1. In each instance, `p` is short for `plot` and `u` short for `using` (column).

We can also plot x-y data simply as:
```
gnuplot> p "phonon_dos_perfect_supercell.dat" u 1:2
```

To plot multiple files, `w`ith `l`ines joining the data points, and with line `t`itles, try:
```
gnuplot> p "phonon_dos_perfect_supercell.dat" u 1:2 w l t "Perfect crystal", "phonon_dos_defective.dat"  u 1:2 w l t "Defective"
gnuplot> set ylabel "Density of States (1/THz)"
gnuplot> set xlabel "Frequency (THz)"
gnuplot> set title "ZrC phonon density of states"
```
To make the plot more presentable, x and y axis labels, and a title, are added. 

Often we may wish to specify the plot range, which we can do by `set xrange [5:20]` or `set xrange [0.5:1.5]` for example. 
To reset the range specification, type for example, `unset xrange`.

Gnuplot can be used to plot functions as well as data. Try for example:
```
gnuplot> p sin(x)
gnuplot> p sin(x), sin(2*x)
gnuplot> p sin(x), sin(2*x), sin(2*x)**2
gnuplot> p sin(x), sin(2*x), sin(2*x)**2, sin(2*x)**2 + 0.5*x
```
To save the image to file, type `set terminal pdf`, followed by `set output "name-of-output-file.pdf"`. 

Examine previously executed commands by typing `history` when `gnuplot` is in interactive mode. 

#### Scripting
To avoid repitition, we can batch commands for gnuplot to execute automatically. To illustrate a `gnuplot` script,
we will write code that fits a polynomial function to some data points, then plots the fitted functions and the data points
in a single figure. To do this make a file called
`approx_plot.gnuplot` containing the following text:
```
f(x)=a+b*x+c*x**2+d*x**3+e*x**4
a=1; b=1; c=1; d=1
fit f(x) "phonon_dos_perfect_supercell.dat" via a, b, c, d, e
set title "Phonon DOS and polynomial approximation"
set xlabel "DOS (1/THz)"
set ylabel "Frequency (THz)"
set term png
set output "approx.png"
p "phonon_dos_perfect_supercell.dat" u 1:2 t "Phonon DOS data points", f(x) t "Polynomial Approximation\n (a bad one)"
```

Execute the script `gnuplot approx_plot.gnuplot`, then open the png file produced, `xdg-open approx.png`.

![some bad approxmiations](.images/approx.png)

Python Jupyter notebooks
-----------------------------------------------------------------------------
In addition to scripting with bash and plotting in the command line, we can also
use Python. Python has rich libraries for numerical, scientific computing and plotting,
that we will use for more detailed analysis of the output from our DFT calculations. 

The python code we will write will use the interactive Jupyter notebooks. You can start a 
Jupyter session by navigating to a directory you want to work in, then typing
`jupyter notebook`. Select a Python3 session and try experimenting. We will cover python code
in the analysis example in future sessions.

------------------------------------------------------------------------------

Summary
-------
Hopefully after this session, you are confident with the important features of the bash shell
needed to navigate the file structure, make and edit files, write script, control jobs and plot
data. This should be all we need to setup and run our first DFT calculation with quantum-espresso.
Try this in the task below.

Task
-------
Compute the electronic density of states of Al in three steps.

#### Step 1:
- Make a directory called `1_al_dos`.
- In `1_al_dos` make a file called `1_al_dft.in`, which will be the parameter file for the DFT
  calculation. Fill it with the following text, which tell quantum-espresso to perform a
self-consistent field electornic structure calculation on bulk aluminium:

```
&CONTROL
   pseudo_dir = '.'
   calculation = 'scf'
/

&SYSTEM
   ibrav = 2      ! Bravais Lattice index -- 2 for fcc
   A = 4.046      ! Lattice parameter in Ang
   nat = 1        ! Num of atoms in cell
   ntyp = 1      ! Num of atom types
   ecutwfc = 29.0 ! Cutoff energy in Rydberg
   occupations='smearing'! Smearing occupation for metals
   degauss = 1.0d-01     ! Smearing width in Ry
   smearing='mp'         ! Methfessel-Paxton smearing
/

&ELECTRONS
   conv_thr = 1.0d-5  ! SCF convergence in Ry
   mixing_beta = 0.7  ! SCF mixing factor
/


ATOMIC_SPECIES
Al  26.982 Al.pbe-nl-kjpaw_psl.1.0.0.UPF

ATOMIC_POSITIONS crystal
Al 0.000000000 0.000000000 0.000000000

K_POINTS automatic
5 5 5 1 1 1          ! k-point sampling grid
```

- If you aren't familiar with DFT calculations, don't worry about the meaning of the 
  keywords for now. This will be discussed in the next lesson.
- In addition to the input parameter file, to run our DFT calculation, we need a
  pseudopotential file. This file will information specifying an effective 
  nuclear potential for each different element. For our Al calculation, download the
  pseudopotential to the `1_al_dos` directory by typing:
  `wget https://www.quantum-espresso.org/upf_files/Al.pbe-nl-kjpaw_psl.1.0.0.UPF`
- Check you are in the directory `1_al_dos` and that it contains
  the pseudopotential and input file. 
- Run the DFT by excuting the quantum-espresso `pw.x` code. To run the calculation, redirect (`<`) the parameter
  file to `pw.x`, and direct the output (`>`) to the file 1_al_dft.out. Run the job in the background (`&`). 
  Together that is:
  `pw.x < 1_al_dft.in > 1_al_dft.out &`
- Inspect the output file using `cat`, then `head` with the appropriate option to look
  at the first 50 lines, then `tail` to look at the last 50 lines. 

### Step 2:

- Next run a non-self-consistent field electronic structure calculation starting from the previous charge density. 
  This calculation has fixed potential and electronic density, but a high k-point sampling of the electronic. Do the 
  calculation by making a file called 
  `2_al_dft.in` filled with the text:

```
&CONTROL
   pseudo_dir = '.'
   calculation = 'nscf'
/

&SYSTEM
   ibrav = 2      ! Bravais Lattice index -- 2 for fcc
   A = 4.046      ! Lattice parameter in Ang
   nat = 1        ! Num of atoms in cell
   ntyp = 1      ! Num of atom types
   ecutwfc = 29.0 ! Cutoff energy in Rydberg
   occupations='smearing'! Smearing occupation for metals
   degauss = 1.0d-01     ! Smearing width in Ry
   smearing='mp'         ! Methfessel-Paxton smearing
/

&ELECTRONS
   conv_thr = 1.0d-5  ! SCF convergence in Ry
   mixing_beta = 0.7  ! SCF mixing factor
/

ATOMIC_SPECIES
Al  26.982 Al.pbe-nl-kjpaw_psl.1.0.0.UPF

ATOMIC_POSITIONS crystal
Al 0.000000000 0.000000000 0.000000000

K_POINTS automatic
15 15 15 1 1 1
```
- Run the `2_al_dft.in` calculation, using `pw.x`, saving the output to an appropriate file name. 
  Inspect the output. 

#### Step 3:
- Finally compute the Al DOS (density of states) by makin a file called `03_dos_al.in` containing
  the text: 
```
&DOS
  degauss = 0.03  ! Gaussian broadening in Ry
  DeltaE = 0.01   ! Bin width eV
 /
```
  and running the DOS calculation with the command `dos.x < 03_dos_al.in > 03_dos_al.out &`.
- Inspect the Al DOS in `pwscf.dos` using cat/head/tail.
- Note the Fermi level and the density of states at the Fermi level.
- Plot the DOS in `pwscf.dos` using `gnuplot`
- Set the energy plot range to be between E_{Fermi}+/-5eV.
- Label axes, and give a title. 
- Try to work out how to shift the energy axis so the Fermi level is at 0 eV, i.e., plot 
  DOS vs E-E_{Fermi}
- Automate plotting in a script to save the plot as a png file.
- Automate running the DFT jobs and plottng in a script.
![al dos](.images/Al_DOS.png)
